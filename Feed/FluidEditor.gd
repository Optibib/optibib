# This file is part of OPTIBIB, a software to optimise enteral feeding for newborn babies
# Copyright (C) 2018 Wiem HASSEN JUTTEAU
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node

signal MaternalMilkValidated(p)

var current_patient

var Utils

func _ready():
	Utils = preload("res://Utils.gd")

const blacklist = ["type",
				"id",
				"brand",
				"product",
				"weight_g",
				"osmolarity_mosm_l_perc_add"]

func reset_connections():
	var button = get_node("VBoxContainer/CenterContainer/ValidateButton")
	if button.is_connected("pressed", self, "_on_ValidateButton_MaternalMilk_pressed"):
		button.disconnect("pressed", self, "_on_ValidateButton_MaternalMilk_pressed")

func _process(delta):
	var button = get_node("VBoxContainer/CenterContainer/ValidateButton")
	if validate():
		button.set_disabled(false)
	else:
		button.set_disabled(true)

func validate():
	if int(get_node("VBoxContainer/GridContainer/volume_ml").get_text()) <= 0:
		return false
	var Fluid = preload("Fluid.gd")
	for e in Fluid.elements:
		if e in blacklist:
			continue
		var text = get_node("VBoxContainer/GridContainer/" + e).text
		if str(float(text)) != text and str(int(text)) != text:
			return false
	return true

func get_default_mother_milk():
		# Try to load default milk
	var Feed = preload("res://Feed/Feed.gd")
	var feed = Feed.new()
	for f in feed.products:
		if f.type == "MotherMilk":
			return f
	return null

func set_mode_mothermilk(p):
	var default_milk = get_default_mother_milk()
	var pre_filled_fields = ["nitrogen_g", "sodium_meq", "potassium_meq", "calcium_mg", "magnesium_mmol", "chloride_mmol", "phosphorus_mg"]
	current_patient = p
	var Fluid = preload("Fluid.gd")
	var title = get_node("VBoxContainer/Title")
	title.set_text("Lait Maternel")
	var path = "VBoxContainer/GridContainer/"
	for e in Fluid.elements:
		if e in blacklist:
			continue
		var v = p.mother_milk.get(e)
		var item = get_node(path + e)
		if default_milk != null:
			item.set_placeholder(str(default_milk.get(e)))
		if v > 0:
			item.set_text(str(p.mother_milk.get(e)))
		else:
			if default_milk != null and e in pre_filled_fields:
				item.set_text(str(default_milk.get(e)))
			else:
				item.set_text("")
	var button = get_node("VBoxContainer/CenterContainer/ValidateButton")
	button.connect("pressed", self, "_on_ValidateButton_MaternalMilk_pressed")

func _on_ValidateButton_MaternalMilk_pressed():
	var Fluid = preload("Fluid.gd")
	var path = "VBoxContainer/GridContainer/"
	for e in Fluid.elements:
		if e in blacklist:
			continue
		current_patient.mother_milk.set(e, Utils.floatit(get_node(path + e).get_text()))
	emit_signal("MaternalMilkValidated", current_patient)
