# This file is part of OPTIBIB, a software to optimise enteral feeding for newborn babies
# Copyright (C) 2018 Wiem HASSEN JUTTEAU
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node2D

var products = []

var Utils

func _init():
	Utils = preload("res://Utils.gd")
	var product_dir = global.user_path() + "products/"
	load_fuilds(product_dir, "res://Feed/products/")

func load_fuilds(dir, default_dir):
	var Fluid = preload("Fluid.gd")
	var d = Directory.new()
	if !d.dir_exists(dir):
		d.make_dir(dir)
	import_missing(default_dir, dir)
	var files = Utils.list_files(dir)
	for f in files:
		var fluid = Fluid.new()
		fluid.load_file(dir + f)
		var already_have_it = false
		for e in products:
			if e.id == fluid.id:
				already_have_it = true
				break
		if already_have_it:
			continue
		products.append(fluid)

func import_missing(src, dst):
	var files = Utils.list_files(src)
	var dir = Directory.new()
	for f in files:
		var dst_path = dst + f
		if !dir.file_exists(dst):
			dir.copy(src + f, dst_path)

func get_id(id):
	for f in products:
		if f.id == id:
			return f
