# Versions

Versions are under the `YY.MM` (Year and Month) which corresponds to the date of release.

## Version 18.08

The very first version of Optibib!
- Basic management of patients
- Add a first set of milks, solutes and fortifiers
- Use mother milks composition
- Compute current osmolarity when information is available
- Build of a nutritional inputs
- Save nutritional inputs as screenshot (to be stored or printed)
- Archive patients
- French interface (only)
