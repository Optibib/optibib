# This file is part of OPTIBIB, a software to optimise enteral feeding for newborn babies
# Copyright (C) 2018 Wiem HASSEN JUTTEAU
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node2D

var id
var first_name
var family_name
var birth_date
var birth_weight
var term_days
var mother_milk
var archive

var items = ["id",
			"first_name",
			"family_name",
			"birth_date",
			"birth_weight",
			"term_days",
			"mother_milk",
			"archive"]

var Utils

func _init():
	Utils = preload("res://Utils.gd")
	var Fluid = preload("res://Feed/Fluid.gd")
	var uuid = preload("res://uuid/uuid.gd")
	id = uuid.v4()
	archive = false
	mother_milk = Fluid.new()
	mother_milk.type = "MotherMilk"

func age_day():
	return int((Utils.now() - birth_date) / 60.0 / 60.0 / 24.0)

func term_w():
	return int(term_days / 7)
	
func term_d():
	return int(term_days) % 7

func term_age_w():
	return int((term_days + age_day()) / 7)

func term_age_d():
	return int(term_days + age_day()) % 7

func load_file(path):
	var Fluid = preload("res://Feed/Fluid.gd")
	var p = Utils.read_json(path)
	if typeof(p) != TYPE_DICTIONARY:
		return
	for i in items:
		if i == "mother_milk":
			continue
		set(i, p[i])
	mother_milk = Fluid.new()
	mother_milk.load_dict(p["mother_milk"])
	
# find a patient with a specific id and return it's file name
func _id_to_file_path(id):
	var patients_dir = global.user_path() + "patients/"
	if !Utils.create_if_needed(patients_dir):
		return
	var files = Utils.list_files(patients_dir)
	for f in files:
		var path = patients_dir + f
		var p = get_script().new()
		p.load_file(path)
		if p.id == id:
			return path
	return FAILED

func save():
	var path = global.user_path() + "patients/" + Utils.date_to_yyyymmdd(birth_date, ".") + " - " +family_name + " - " + first_name + ".json"
	var previous_path = _id_to_file_path(id)
	if typeof(previous_path) == TYPE_STRING and previous_path != path:
		var dir = Directory.new()
		dir.rename(previous_path, path)
	save_file(path)
	
func save_file(path):
	var p = {}
	for i in items:
		if i == "mother_milk":
			continue
		p[i] = get(i)
	p["mother_milk"] = mother_milk.get_dict()
	var f = File.new()
	if f.open(path, File.WRITE) != OK:
		return
	f.store_string(to_json(p))
	f.close()
	return OK
