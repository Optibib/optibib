# This file is part of OPTIBIB, a software to optimise enteral feeding for newborn babies
# Copyright (C) 2018 Wiem HASSEN JUTTEAU
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Container

signal patientCreated(p)
signal patientUpdated(p)

var current_modified_patient = null

var Utils

func _ready():
	Utils = preload("res://Utils.gd")

func _process(delta):
	var button = get_node("VBoxContainer/CenterContainer/Create")
	if validate():
		button.set_disabled(false)
	else:
		button.set_disabled(true)

func validate():
	var first_name = get_node("VBoxContainer/GridContainer/FirstnameEdit").text
	if first_name.length() == 0:
		return false
	var family_name = get_node("VBoxContainer/GridContainer/FamilynameEdit").text
	if family_name.length() == 0:
		return false
	var birth_date = get_node("VBoxContainer/GridContainer/BirthDateEdit").text
	if !Utils.ddmmyyyy_to_date(birth_date):
		return false
	var weigth_txt = get_node("VBoxContainer/GridContainer/BirthWeightEdit").text
	var weigth = Utils.floatit(weigth_txt)
	if ! (weigth > 0 and weigth < 10):
		return false
	var term_week_txt = get_node("VBoxContainer/GridContainer/HBoxContainer/TermWeeksEdit").text
	var term_weeks = int(term_week_txt)
	if term_weeks <= 0 or term_weeks >= 50:
		return false
	var term_days_txt = get_node("VBoxContainer/GridContainer/HBoxContainer/TermDays").text
	var term_days = int(term_days_txt)
	# we cannot make the difference between a bad integer conversion and a real 0
	if term_days == 0 and term_days_txt != "0":
		return false
	if term_days < 0 or term_days >= 7:
		return false
	return true

func load_view_into_patient(p):
	p.first_name = get_node("VBoxContainer/GridContainer/FirstnameEdit").text
	p.family_name = get_node("VBoxContainer/GridContainer/FamilynameEdit").text
	p.birth_date = Utils.ddmmyyyy_to_date(get_node("VBoxContainer/GridContainer/BirthDateEdit").text)
	p.birth_weight = Utils.floatit(get_node("VBoxContainer/GridContainer/BirthWeightEdit").text)
	var weeks = int(get_node("VBoxContainer/GridContainer/HBoxContainer/TermWeeksEdit").text)
	var days = int(get_node("VBoxContainer/GridContainer/HBoxContainer/TermDays").text)
	p.term_days = weeks * 7 + days

func load_patient_into_view(p):
	get_node("VBoxContainer/GridContainer/FirstnameEdit").set_text(p.first_name)
	get_node("VBoxContainer/GridContainer/FamilynameEdit").set_text(p.family_name)
	get_node("VBoxContainer/GridContainer/BirthDateEdit").set_text(Utils.date_to_ddmmyyyy(p.birth_date, "/"))
	get_node("VBoxContainer/GridContainer/BirthWeightEdit").set_text(str(p.birth_weight))
	get_node("VBoxContainer/GridContainer/HBoxContainer/TermWeeksEdit").set_text(str(p.term_w()))
	get_node("VBoxContainer/GridContainer/HBoxContainer/TermDays").set_text(str(p.term_d()))

func reset_view():
	get_node("VBoxContainer/GridContainer/FirstnameEdit").set_text("")
	get_node("VBoxContainer/GridContainer/FamilynameEdit").set_text("")
	get_node("VBoxContainer/GridContainer/BirthDateEdit").set_text("")
	get_node("VBoxContainer/GridContainer/BirthWeightEdit").set_text("")
	get_node("VBoxContainer/GridContainer/HBoxContainer/TermWeeksEdit").set_text("")
	get_node("VBoxContainer/GridContainer/HBoxContainer/TermDays").set_text("")

func reset_connections():
	var button = get_node("VBoxContainer/CenterContainer/Create")
	if button.is_connected("pressed", self, "_on_Create_pressed"):
		button.disconnect("pressed", self, "_on_Create_pressed")
	if button.is_connected("pressed", self, "_on_Update_pressed"):
		button.disconnect("pressed", self, "_on_Update_pressed")

func create_patient_mode():
	reset_view()
	var button = get_node("VBoxContainer/CenterContainer/Create")
	button.set_text("Créer")
	button.connect("pressed", self, "_on_Create_pressed")

func _on_Create_pressed():
	var Patient = preload("Patient.gd")
	# Build new patient
	var p = Patient.new()
	load_view_into_patient(p)
	var button = get_node("VBoxContainer/CenterContainer/Create")
	button.disconnect("pressed", self, "_on_Create_pressed")
	emit_signal("patientCreated", p)

func update_patient_mode(p):
	current_modified_patient = p
	load_patient_into_view(p)
	var button = get_node("VBoxContainer/CenterContainer/Create")
	button.set_text("Mise à jour")
	button.connect("pressed", self, "_on_Update_pressed")

func _on_Update_pressed():
	load_view_into_patient(current_modified_patient)
	var button = get_node("VBoxContainer/CenterContainer/Create")
	button.disconnect("pressed", self, "_on_Update_pressed")
	emit_signal("patientUpdated", current_modified_patient)
