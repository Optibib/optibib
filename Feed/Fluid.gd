# This file is part of OPTIBIB, a software to optimise enteral feeding for newborn babies
# Copyright (C) 2018 Wiem HASSEN JUTTEAU
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node2D

var type
var id
var brand
var product
var volume_ml
var weight_g
var protide_g
var nitrogen_g
var carbohydrate_g
var lipides_g
var total_calories_kcal
var sodium_meq
var potassium_meq
var calcium_mg
var magnesium_mmol
var chloride_mmol
var phosphorus_mg
var osmolarity_mosm_l_perc_add

const elements=["type",
				"id",
				"brand",
				"product",
				"volume_ml",
				"weight_g",
				"protide_g",
				"nitrogen_g",
				"carbohydrate_g",
				"lipides_g",
				"total_calories_kcal",
				"sodium_meq",
				"potassium_meq",
				"calcium_mg",
				"magnesium_mmol",
				"chloride_mmol",
				"phosphorus_mg",
				"osmolarity_mosm_l_perc_add"]

func _init():
	for e in elements:
		set(e, 0)
	var uuid = preload("res://uuid/uuid.gd")
	type = "None"
	id = uuid.v4()
	brand = ""
	product = ""

func load_dict(dict):
	for e in elements:
		if dict.has(e):
			set(e, dict[e])

func load_file(path):
	var Utils = preload("res://Utils.gd")
	var f = Utils.read_json(path)
	if typeof(f) != TYPE_DICTIONARY:
		return
	load_dict(f)

func get_dict():
	var p = {}
	for e in elements:
		p[e] = get(e)
	return p

func save_file(path):
	var p = get_dict()
	var f = File.new()
	if f.open(path, File.WRITE) != OK:
		return
	f.store_string(to_json(p))
	f.close()
	return OK
