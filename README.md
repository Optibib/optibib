# Optibib, a software to optimise enteral feeding for newborn babies

This software helps doctors computing nutritional input for premature babies.

This is still young and targets french doctors only for the moment. We can expect to have support in other languages in future versions.

To get more information about Optibib, check the website: [optibib.fr](https://www.optibib.fr/).

# Features

At the moment, a very basic list of functionalities are available:
- Basic patient management
- Basic build of nutritional inputs
- Add a first set of milks, solutes and fortifiers
- Use mother milks composition
- Compute current osmolarity when information is available
- Save nutritional inputs as screenshot (to be stored or printed)
- Archive patients (still keep the file)

# Download

To download Optibib, check [optibib.fr](https://www.optibib.fr/).

# Building

The project has been created using [Godot v3 engine](godotengine.org/).

In order to build Optibib, install Godot engine, open the project using Godot and go to project -> export.

Godot supports a number of platforms, check their [export documentation](https://godot.readthedocs.io/en/3.0/getting_started/workflow/export/exporting_projects.html) for instructions.

# Project Genesis

Optibib was created by Wiem HASSEN JUTTEAU (Pediatric Doctor) with the help of Jérôme JUTTEAU (Software Engineer).
In 2018, the project started as a simple spread sheet to help doctors making repetitive calculation in hospital departments (neonatal and premature babies). It was already helpful and Wiem already made a better usage of devices which analyse mother milk composition. As milks, fortifiers and other products were needed, the spreadsheet became more and more complex to use and adapt. So Wiem decided to create a real software to continue her mission.

# License

> OPTIBIB, a software to optimise enteral feeding for newborn babies
> Copyright (C) 2018 Wiem HASSEN JUTTEAU
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program.  If not, see <https://www.gnu.org/licenses/>.
