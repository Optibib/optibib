# This file is part of OPTIBIB, a software to optimise enteral feeding for newborn babies
# Copyright (C) 2018 Wiem HASSEN JUTTEAU
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Container

var feed
var today_weight = 0.0

var titles
var expander
var all_items = []
var total_item
var total_kg_day_item
var total_kg_hour_item
var units
var units2
var Utils

func _ready():
	if OS.get_name() == "HTML5":
		$VBox/ToolboxActions/Print.hide()
	
	Utils = preload("res://Utils.gd")
	var Feed = preload("res://Feed/Feed.gd")
	feed = Feed.new()
	var milk_menu = get_node("VBox/ToolboxActions/Milks/MilkPopup")
	var fortifier_menu = get_node("VBox/ToolboxActions/Fortifiers/FortifiersPopup")
	var solute_menu = get_node("VBox/ToolboxActions/Solutes/SolutesPopup")
	var i = 0
	for p in feed.products:
		if p.type == "Milk":
			milk_menu.add_item(p.brand + " - " + p.product, i)
		elif p.type == "Fortifier":
			fortifier_menu.add_item(p.brand + " - " + p.product, i)
		elif p.type == "Solute":
			solute_menu.add_item(p.brand + " - " + p.product, i)
		i += 1
	
	var ItemView = preload("res://Feed/ItemView.gd")
	titles = ItemView.new()
	titles.set_titles()
	expander = ItemView.new()
	expander.set_expander()
	total_item = ItemView.new()
	total_item.set_empty()
	total_item.title.set_text("Total")
	total_kg_day_item = ItemView.new()
	total_kg_day_item.set_empty()
	total_kg_day_item.title.set_text("Total/Kg/j")
	for i in ItemView.all_items:
		total_kg_day_item.get(i).add_color_override("font_color", Color(1, 0, 0, 1))
	total_kg_hour_item = ItemView.new()
	total_kg_hour_item.set_empty()
	total_kg_hour_item.title.set_text("Total/Kg/h")
	total_kg_hour_item.carbohydrate_g.add_color_override("font_color", Color(1, 0, 0, 1))
	units = ItemView.new()
	units.set_units()
	units2 = ItemView.new()
	units2.set_units()
	var g = get_node("/root/global").global_patient()
	var first_name = get_node("VBox/Scroll/Vert/PatientInfos/Block1/FirstName")
	first_name.set_text(g.first_name)
	var family_name = get_node("VBox/Scroll/Vert/PatientInfos/Block1/FamilyName")
	family_name.set_text(g.family_name)
	var birth_date = get_node("VBox/Scroll/Vert/PatientInfos/Block4/BirthDate")
	birth_date.set_text(Utils.date_to_ddmmyyyy(g.birth_date, "/"))
	var today_date = get_node("VBox/Scroll/Vert/PatientInfos/Block2/TodayDate")
	today_date.set_text(Utils.now_ddmmyyyy("/"))
	var age = get_node("VBox/Scroll/Vert/PatientInfos/Block4/Age")
	age.set_text(str(g.age_day()) + "j")
	var am_age = get_node("VBox/Scroll/Vert/PatientInfos/Block22/TermAge")
	am_age.set_text(str(g.term_age_w()) + "s + " + str(g.term_age_d()) + "j")
	
	# Does Mothermilk informations has been filled?
	var mothermilk_button = get_node("VBox/ToolboxActions/MotherMilk")
	if g.mother_milk.volume_ml == 0:
		mothermilk_button.disabled = true
	else:
		mothermilk_button.disabled = false
	redraw()

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func redraw():
	# Clear "All" subnodes
	var all = get_node("VBox/Scroll/Vert/All")
	for c in all.get_children():
		all.remove_child(c)
	# Configure number of columns
	all.set_columns(grid_size())
	for i in all_items:
		if !i.is_connected("refresh", self, "refresh"):
			i.connect("refresh", self, "refresh")
		if !i.is_connected("close", self, "_close_itemview"):
			i.connect("close", self, "_close_itemview")
	# Add each items
	#add_separators(all)
	for line in ["close", "title", "brand", "product", "volume_ml", "protide_g", "nitrogen_g",
				 "carbohydrate_g", "lipides_g", "total_calories_kcal", "total_calories_non_prot_kcal",
				 "sodium_meq", "potassium_meq", "calcium_mg", "magnesium_mmol", "chloride_mmol",
				 "phosphorus_mg"]:
		all.add_child(titles.get(line))
		all.add_child(units.get(line))
		all.add_child(VSeparator.new())
		for i in all_items:
			if i.fluid.type == "Milk" or i.fluid.type == "MotherMilk":
				all.add_child(i.get(line))
				all.add_child(VSeparator.new())
		for i in all_items:
			if i.fluid.type == "Solute":
				all.add_child(i.get(line))
				all.add_child(VSeparator.new())
		for i in all_items:
			if i.fluid.type == "Bag":
				all.add_child(i.get(line))
				all.add_child(VSeparator.new())
		for i in all_items:
			if i.fluid.type == "Fortifier":
				all.add_child(i.get(line))
				all.add_child(VSeparator.new())
		all.add_child(expander.get(line))
		all.add_child(VSeparator.new())
		all.add_child(total_item.get(line))
		all.add_child(VSeparator.new())
		all.add_child(total_kg_day_item.get(line))
		all.add_child(VSeparator.new())
		all.add_child(total_kg_hour_item.get(line))
		all.add_child(units2.get(line))
		add_separators(all)
	refresh()

func grid_size():
	return (6 + all_items.size()) * 2 - 1

func add_separators(all):
	for i in range(0, grid_size()):
		all.add_child(HSeparator.new())

func refresh():
	# Before all, refresh Fortifiers which are altered by milk change
	var osmo_v = 300
	for p in all_items:
		if p.fluid.type == "Fortifier":
			p.refresh_fortifier()
			osmo_v += p.fluid.osmolarity_mosm_l_perc_add * Utils.floatit(p.percentage.get_text())
	# Update Osmolarity
	var osmo = get_node("VBox/Scroll/Vert/PatientInfos/Block2/Osmolarity")
	osmo.set_text(Utils.float_print(osmo_v) + " mOsm/L/%Add")
	# Refresh totals
	var ItemView = preload("res://Feed/ItemView.gd")
	var weight = Utils.floatit(get_node("VBox/Scroll/Vert/PatientInfos/Block22/WeightEdit").get_text())
	for p in ItemView.all_items:
		var total = 0.0
		for i in all_items:
			total += Utils.floatit(i.get(p).get_text())
		total_item.get(p).set_text(Utils.float_print(total))
#		# Hurray for float comparaisons (?)
		if weight == 0.0:
			total_kg_day_item.get(p).set_text("")
			total_kg_hour_item.get(p).set_text("")
			continue
		var total_kg_day = total / weight
		total_kg_day_item.get(p).set_text(Utils.float_print(total_kg_day))
		var total_kg_hour = total_kg_day / 24.0
		total_kg_hour_item.get(p).set_text(Utils.float_print(total_kg_hour))

func _on_WeightEdit_text_changed(text):
	today_weight = Utils.floatit(text)
	refresh()

func _on_Exit_pressed():
	get_node("/root/global").goto_patients()

func _on_Milks_pressed():
	get_node("VBox/ToolboxActions/Milks/MilkPopup").popup()

func _on_Fortifiers_pressed():
	get_node("VBox/ToolboxActions/Fortifiers/FortifiersPopup").popup()

func _on_Solutes_pressed():
	get_node("VBox/ToolboxActions/Solutes/SolutesPopup").popup()

func _on_Print_pressed():
	var g = get_node("/root/global").global_patient()
	var filename = Utils.now_yyyymmddhhmm(".") + " - " + g.family_name + " - " + g.first_name + ".png"
	var path_prefix = global.user_path_real() + "nutritional-intake/"
	Utils.create_if_needed(path_prefix)
	var path = path_prefix + filename
	print(path)
	var screen = get_node("VBox/Scroll/Vert")
	Utils.screenshot(screen, path)
	var os = OS.get_name()
	if os == "OSX":
		var output = []
		var pid = OS.execute('open', [path], true, output)
	#elif os == "Windows":
	else:
		OS.shell_open(path)

func _on_MotherMilk_pressed():
	var patient = get_node("/root/global").global_patient()
	var ItemView = preload("res://Feed/ItemView.gd")
	var new_milk = ItemView.new()
	new_milk.set_mothermilk(patient.mother_milk)
	all_items.append(new_milk)
	var button1 = get_node("VBox/ToolboxActions/Milks")
	button1.disabled = true
	var button2 = get_node("VBox/ToolboxActions/MotherMilk")
	button2.disabled = true
	redraw()

func _on_Fluid_id_pressed(ID):
	# Get Fluid
	var fluid = feed.products[ID]
	var item_type = fluid.type
	# Create nwe ItemView
	var ItemView = preload("res://Feed/ItemView.gd")
	var new_item = ItemView.new()
	# Configure ItemView depending of the type
	if item_type == "Milk":
		new_item.set_milk(fluid)
		var button1 = get_node("VBox/ToolboxActions/Milks")
		button1.disabled = true
		var button2 = get_node("VBox/ToolboxActions/MotherMilk")
		button2.disabled = true
	elif item_type == "Fortifier":
		new_item.set_fortifier(fluid, all_items)
	elif item_type == "Solute":
		var button = get_node("VBox/ToolboxActions/Solutes")
		button.disabled = true
		new_item.set_solute(fluid)
	# Add new ItemView to all items
	all_items.append(new_item)
	redraw()

func _on_Bag_pressed():
	var ItemView = preload("res://Feed/ItemView.gd")
	var item = ItemView.new()
	item.set_bag()
	all_items.append(item)
	var bag = get_node("VBox/ToolboxActions/Bag")
	bag.disabled = true
	redraw()

func _close_itemview(itemview):
	var g = get_node("/root/global").global_patient()
	var type = itemview.fluid.type
	if type == "Milk" or type == "MotherMilk":
		var mb = get_node("VBox/ToolboxActions/Milks")
		mb.disabled = false
		if g.mother_milk.volume_ml > 0:
			var mmb = get_node("VBox/ToolboxActions/MotherMilk")
			mmb.disabled = false
	if type == "Solute":
		var button = get_node("VBox/ToolboxActions/Solutes")
		button.disabled = false
	if type == "Bag":
		var bag = get_node("VBox/ToolboxActions/Bag")
		bag.disabled = false
	all_items.erase(itemview)
	redraw()
