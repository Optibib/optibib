# This file is part of OPTIBIB, a software to optimise enteral feeding for newborn babies
# Copyright (C) 2018 Wiem HASSEN JUTTEAU
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Container

var patients = []

var Utils

func _ready():
	Utils = preload("res://Utils.gd")
	var window_patient = get_node("CreationPopup/Create")
	window_patient.connect("patientCreated", self, "_on_patient_created")
	window_patient.connect("patientUpdated", self, "_on_patient_updated")
	load_patients()
	var window_mothermilk = get_node("MotherMilkPopup/FluidEditor")
	window_mothermilk.connect("MaternalMilkValidated", self, "_on_mothermilk_updated")
	if OS.get_name() == "HTML5":
		$TopContainer/TopPannel/ProductCenter.hide()
	refresh()

class PatientSorter:
	static func sort(a, b):
		return a.birth_date > b.birth_date

func refresh():
	var listing = get_node("TopContainer/Scroll/AllPatients")
	for node in listing.get_children():
		node.queue_free()
	
	if patients.size() == 0:
		return
	# Add labels
	var first_name_l = Label.new()
	first_name_l.set_text("Prénom")
	first_name_l.set_align(HALIGN_CENTER)
	listing.add_child(first_name_l)
	listing.add_child(VSeparator.new())
	# Add family name
	var family_name_l = Label.new()
	family_name_l.set_text("Nom")
	family_name_l.set_align(HALIGN_CENTER)
	listing.add_child(family_name_l)
	listing.add_child(VSeparator.new())
	# Add birth date
	var birth_date_l = Label.new()
	birth_date_l.set_text("Date de naissance")
	birth_date_l.set_align(HALIGN_CENTER)
	listing.add_child(birth_date_l)
	listing.add_child(VSeparator.new())
	# Add age
	var age_l = Label.new()
	age_l.set_text("Age")
	age_l.set_align(HALIGN_CENTER)
	listing.add_child(age_l)
	listing.add_child(VSeparator.new())
	# Add birth weight
	var birth_weight_l = Label.new()
	birth_weight_l.set_text("Poids de naissance")
	birth_weight_l.set_align(HALIGN_CENTER)
	listing.add_child(birth_weight_l)
	listing.add_child(VSeparator.new())
	# Add Term date
	var term_l = Label.new()
	term_l.set_text("Terme de naissance")
	term_l.set_align(HALIGN_CENTER)
	listing.add_child(term_l)
	listing.add_child(VSeparator.new())
	# Add Term age
	var term_now_l = Label.new()
	term_now_l.set_text("Terme actuel")
	term_now_l.set_align(HALIGN_CENTER)
	listing.add_child(term_now_l)
	listing.add_child(VSeparator.new())
	# Add empty labels
	for i in range(0, 4):
		listing.add_child(Label.new())

	patients.sort_custom(PatientSorter, "sort")
	# Fill patient list
	for p in patients:
		# Add seperators
		for i in range(0, 18):
			var sep = HSeparator.new()
			listing.add_child(sep)
		# Add first name
		var first_name = Label.new()
		first_name.set_text(p.first_name)
		first_name.set_h_size_flags(Control.SIZE_EXPAND | Control.SIZE_FILL)
		listing.add_child(first_name)
		listing.add_child(VSeparator.new())
		# Add family name
		var family_name = Label.new()
		family_name.set_text(p.family_name)
		family_name.set_h_size_flags(Control.SIZE_EXPAND | Control.SIZE_FILL)
		listing.add_child(family_name)
		listing.add_child(VSeparator.new())
		# Add birth date
		var birth_date = Label.new()
		birth_date.set_text(Utils.date_to_ddmmyyyy(p.birth_date, "/"))
		birth_date.set_h_size_flags(2)
		listing.add_child(birth_date)
		listing.add_child(VSeparator.new())
		# Add age
		var age = Label.new()
		age.set_text(str(p.age_day()) + "j")
		age.set_h_size_flags(2)
		listing.add_child(age)
		listing.add_child(VSeparator.new())
		# Add birth weight
		var birth_weight = Label.new()
		birth_weight.set_text(str(p.birth_weight) + " kg")
		birth_weight.set_h_size_flags(Control.SIZE_EXPAND | Control.SIZE_FILL)
		listing.add_child(birth_weight)
		listing.add_child(VSeparator.new())
		# Add Term date
		var term = Label.new()
		term.set_text(str(p.term_w()) + "s + " + str(p.term_d()) + "j")
		term.set_h_size_flags(Control.SIZE_EXPAND | Control.SIZE_FILL)
		listing.add_child(term)
		listing.add_child(VSeparator.new())
		# Add Term age
		var term_now = Label.new()
		term_now.set_text(str(p.term_age_w()) + "s + " + str(p.term_age_d()) + "j")
		term_now.set_h_size_flags(Control.SIZE_EXPAND | Control.SIZE_FILL)
		listing.add_child(term_now)
		listing.add_child(VSeparator.new())
		# Add open button
		var open = Button.new()
		open.set_text("Apports")
		open.connect("pressed", self, "button_patient_open", [p])
		open.set_h_size_flags(Control.SIZE_FILL)
		listing.add_child(open)
		# Add mother milk button
		var mother_milk = Button.new()
		mother_milk.set_text("Lait Maternel")
		mother_milk.connect("pressed", self, "button_patient_mother_milk_edit", [p])
		mother_milk.set_h_size_flags(Control.SIZE_FILL)
		listing.add_child(mother_milk)
		# Add edit button
		var edit = Button.new()
		edit.set_text("Modifier Patient")
		edit.connect("pressed", self, "button_patient_edit", [p])
		edit.set_h_size_flags(Control.SIZE_FILL)
		listing.add_child(edit)
		# Add archive button
		var archive = Button.new()
		archive.set_text("Archiver")
		archive.connect("pressed", self, "button_patient_archive", [p])
		archive.set_h_size_flags(Control.SIZE_FILL)
		listing.add_child(archive)
	# Add seperators
	for i in range(0, 18):
		var sep = HSeparator.new()
		listing.add_child(sep)
	
func button_patient_open(patient):
	get_node("/root/global").goto_menu(patient)

func load_patients():
	var Patient = preload("Patient.gd")
	var patients_dir = global.user_path() + "patients/"
	if !Utils.create_if_needed(patients_dir):
		print("failed to create patient dir")
		print(patients_dir)
		return
	var files = Utils.list_files(patients_dir)
	for f in files:
		var p = Patient.new()
		p.load_file(patients_dir + f)
		var already_have_it = false
		for e in patients:
			if e.id == p.id:
				already_have_it = true
				break
		if already_have_it:
			continue
		patients.append(p)

func _on_NewPatient_pressed():
	var window = get_node("CreationPopup/Create")
	window.create_patient_mode()
	var popup = get_node("CreationPopup")
	popup.set_title("Ajout d'un nouveau patient")
	popup.popup_centered()

func _on_patient_created(p):
	patients.append(p)
	p.save()
	get_node("CreationPopup").set_visible(false)
	refresh()

func button_patient_archive(patient):
	patient.archive = true
	patient.save()
	patients.erase(patient)
	refresh()

func button_patient_edit(patient):
	var window = get_node("CreationPopup/Create")
	window.update_patient_mode(patient)
	var popup = get_node("CreationPopup")
	popup.set_title("Modifiation patient")
	popup.popup_centered()

func _on_patient_updated(new_p):
	new_p.save()
	get_node("CreationPopup").set_visible(false)
	refresh()

func _on_CreationPopup_hide():
	get_node("CreationPopup/Create").reset_connections()
	
func button_patient_mother_milk_edit(p):
	var window = get_node("MotherMilkPopup/FluidEditor")
	window.set_mode_mothermilk(p)
	var popup = get_node("MotherMilkPopup")
	popup.set_title("Modifier le lait maternel")
	popup.popup_centered()

func _on_MotherMilkPopup_hide():
	var window = get_node("MotherMilkPopup/FluidEditor")
	window.reset_connections()

func _on_mothermilk_updated(new_p):
	new_p.save()
	get_node("MotherMilkPopup").set_visible(false)

func _on_ProductCenter_pressed():
	var path = global.user_path_real() + "products"
	var os = OS.get_name()
	var dir = Directory.new()
	if !dir.dir_exists(path):
		# Just load presets to create dirs and copy files
		var Feed = preload("res://Feed/Feed.gd")
		Feed.new()
	if os == "OSX":
		var output = []
		var pid = OS.execute('open', [path], true, output)
	#elif os == "Windows":
	else:
		OS.shell_open(path)

func _on_About_pressed():
	get_node("AboutPopup").popup_centered()

func _on_About_meta_clicked(meta):
	OS.shell_open(meta)
