# This file is part of OPTIBIB, a software to optimise enteral feeding for newborn babies
# Copyright (C) 2018 Wiem HASSEN JUTTEAU
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

static func list_files(path):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	if dir.list_dir_begin() != OK:
		return
	while true:
		var f = dir.get_next()
		if f == "":
			break
		if f.begins_with("."):
			continue
		files.append(f)
	dir.list_dir_end()
	return files

static func create_if_needed(path):
	var d = Directory.new()
	if !d.dir_exists(path):
		d.make_dir(path)
	return d.dir_exists(path)

static func read_json(path):
	var f = File.new()
	f.open(path, File.READ)
	var json = f.get_as_text()
	f.close()
	var p = JSON.parse(json)
	if typeof(p.result) != TYPE_DICTIONARY:
		return
	return p.result

# Validate dd/mm/yyyy format
static func ddmmyyyy_to_date(date):
	if date.length() != 10:
		return false
	if date[2] + date[5] != "//":
		return false
	var day = int(date[0] + date[1])
	if day <= 0 or day > 31:
		return false
	var month = int(date[3] + date[4])
	if month <= 0 or month > 12:
		return false
	var year = int(date[6] + date[7] + date[8] + date[9])
	if year <= 0 or year < 2018:
		return false
	var d = {
		"year": year,
		"month": month,
		"day": day,
		"hour": 0,
		"minute": 0,
		"second": 0
	}
	return OS.get_unix_time_from_datetime(d)

static func date_to_ddmmyyyy(date, sep):
	var d = OS.get_datetime_from_unix_time(date)
	var day =  str(d["day"])
	if day.length() == 1:
		day = "0" + day
	var month =  str(d["month"])
	if month.length() == 1:
		month = "0" + month
	return day + sep + month + sep + str(d["year"])

static func now_ddmmyyyy(sep):
	return date_to_ddmmyyyy(now(), sep)

static func date_to_yyyymmdd(date, sep):
	var d = OS.get_datetime_from_unix_time(date)
	var day =  str(d["day"])
	if day.length() == 1:
		day = "0" + day
	var month =  str(d["month"])
	if month.length() == 1:
		month = "0" + month
	return str(d["year"]) + sep + month + sep + day

static func now():
	# OS.get_unix_time() -> UTC
	return OS.get_unix_time_from_datetime(OS.get_datetime())

static func date_to_yyyymmddhhmm(date, sep):
	var d = OS.get_datetime_from_unix_time(date)
	var part1 = date_to_yyyymmdd(date, sep)
	return part1 + sep + str(d["hour"]) + sep + str(d["minute"])

static func now_yyyymmddhhmm(sep):
	return date_to_yyyymmddhhmm(now(), sep)

static func screenshot(node, path):
	# Retrieve the captured Image using get_data()
	var img = node.get_viewport().get_texture().get_data()
	# Also remember to flip the texture (because it's flipped)
	img.flip_y()
	img.save_png(path)

static func float_print(value):
	# return string representation of float with max 2 decimal after coma
	return str(int(value * 100)/100.0)

static func floatit(string):
	return float(string.replace(",", "."))

func version_enabled():
	var result = 0
	var http = HTTPClient.new() # Create the Client

	result = http.connect_to_host("optibib.fr", 80) # Connect to host/port
	if result != OK:
		return false
	while http.get_status() == HTTPClient.STATUS_CONNECTING or http.get_status() == HTTPClient.STATUS_RESOLVING:
		http.poll()
		OS.delay_msec(10)

	if http.get_status() != HTTPClient.STATUS_CONNECTED:
		return false

	var headers = [
		"User-Agent: Pirulo/1.0 (Godot)",
		"Accept: */*"
	]
	result = http.request(HTTPClient.METHOD_GET, "/releases/18.11/web/available", headers)
	if result != OK:
		return false

	while http.get_status() == HTTPClient.STATUS_REQUESTING:
		http.poll()
		OS.delay_msec(10)

	var status = http.get_status()
	if not status in [HTTPClient.STATUS_BODY, HTTPClient.STATUS_CONNECTED]:
		return false
	if not http.has_response():
		return false
	headers = http.get_response_headers_as_dictionary()
	if http.get_response_code() != 200:
		return false

	return true
