# This file is part of OPTIBIB, a software to optimise enteral feeding for newborn babies
# Copyright (C) 2018 Wiem HASSEN JUTTEAU
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node

signal refresh()
signal close(itemview)

var fluid

# Those are UI items, not part of the model
var title
var close
var brand
var product
var volume_ml
var percentage
var protide_g
var nitrogen_g
var carbohydrate_g
var lipides_g
var total_calories_kcal
var total_calories_non_prot_kcal
var sodium_meq
var potassium_meq
var calcium_mg
var magnesium_mmol
var chloride_mmol
var phosphorus_mg

var Utils

# reference to all products for fortifier case
# it need to have a milk volume (if aded)
var all_items_ref = []

const all_items = ["volume_ml",
					"protide_g",
					"nitrogen_g",
					"carbohydrate_g",
					"lipides_g",
					"total_calories_kcal",
					"total_calories_non_prot_kcal",
					"sodium_meq",
					"potassium_meq",
					"calcium_mg",
					"magnesium_mmol",
					"chloride_mmol",
					"phosphorus_mg"]

func _init():
	Utils = preload("res://Utils.gd")

func set_empty():
	var Fluid = preload("res://Feed/Fluid.gd")
	fluid = Fluid.new()
	title = Label.new()
	close = Label.new()
	brand = Label.new()
	product = Label.new()
	volume_ml = Label.new()
	protide_g = Label.new()
	nitrogen_g = Label.new()
	carbohydrate_g = Label.new()
	lipides_g = Label.new()
	total_calories_kcal = Label.new()
	total_calories_non_prot_kcal = Label.new()
	sodium_meq = Label.new()
	potassium_meq = Label.new()
	calcium_mg = Label.new()
	magnesium_mmol = Label.new()
	chloride_mmol = Label.new()
	phosphorus_mg = Label.new()

func set_milk(milk):
	set_empty()
	fluid = milk
	title.set_text("Lait")
	close = Button.new()
	close.set_text("❌X")
	close.add_color_override("font_color", Color(1, 0, 0, 1))
	close.connect("pressed", self, "_close")
	close.set_h_size_flags(Control.SIZE_SHRINK_CENTER)
	brand.set_text(milk.brand)
	product.set_text(milk.product)
	volume_ml = LineEdit.new()
	volume_ml.placeholder_text = "en ml"
	volume_ml.connect("text_changed", self, "_volume_changed")

func set_mothermilk(milk):
	set_empty()
	fluid = milk
	title.set_text("Lait Maternel")
	close = Button.new()
	close.set_text("❌X")
	close.add_color_override("font_color", Color(1, 0, 0, 1))
	close.connect("pressed", self, "_close")
	close.set_h_size_flags(Control.SIZE_SHRINK_CENTER)
	volume_ml = LineEdit.new()
	volume_ml.placeholder_text = "en ml"
	volume_ml.connect("text_changed", self, "_volume_changed")

func set_fortifier(fortifier, all_items):
	set_empty()
	fluid = fortifier
	all_items_ref = all_items
	title = HBoxContainer.new()
	var label = Label.new()
	label.set_text("Fortifiant: ")
	title.add_child(label)
	close = Button.new()
	close.set_text("❌X")
	close.add_color_override("font_color", Color(1, 0, 0, 1))
	close.connect("pressed", self, "_close")
	close.set_h_size_flags(Control.SIZE_SHRINK_CENTER)
	percentage = LineEdit.new()
	percentage.set_placeholder("0")
	percentage.connect("text_changed", self, "_percentage_changed")
	title.add_child(percentage)
	var percentage_label = Label.new()
	percentage_label.set_text("%")
	title.add_child(percentage_label)
	brand.set_text(fortifier.brand)
	product.set_text(fortifier.product)
	volume_ml = Label.new()

func set_solute(solute):
	set_empty()
	fluid = solute
	title.set_text("Soluté")
	close = Button.new()
	close.set_text("❌X")
	close.add_color_override("font_color", Color(1, 0, 0, 1))
	close.connect("pressed", self, "_close")
	close.set_h_size_flags(Control.SIZE_SHRINK_CENTER)
	brand.set_text(solute.brand)
	product.set_text(solute.product)
	volume_ml = LineEdit.new()
	volume_ml.placeholder_text = "en ml"
	volume_ml.connect("text_changed", self, "_volume_changed")

func set_bag():
	set_empty()
	fluid.type = "Bag"
	title.set_text("Poche")
	close = Button.new()
	close.set_text("❌X")
	close.add_color_override("font_color", Color(1, 0, 0, 1))
	close.connect("pressed", self, "_close")
	close.set_h_size_flags(Control.SIZE_SHRINK_CENTER)
	volume_ml = LineEdit.new()
	volume_ml.connect("text_changed", self, "_bag_changed")
	volume_ml.placeholder_text = "en ml"
	protide_g = LineEdit.new()
	protide_g.connect("text_changed", self, "_bag_changed")
	protide_g.placeholder_text = "g"
	nitrogen_g = LineEdit.new()
	nitrogen_g.connect("text_changed", self, "_bag_changed")
	nitrogen_g.placeholder_text = "g"
	carbohydrate_g = LineEdit.new()
	carbohydrate_g.connect("text_changed", self, "_bag_changed")
	carbohydrate_g.placeholder_text = "g"
	lipides_g = LineEdit.new()
	lipides_g.connect("text_changed", self, "_bag_changed")
	lipides_g.placeholder_text = "g"
	total_calories_kcal = LineEdit.new()
	total_calories_kcal.connect("text_changed", self, "_bag_changed")
	total_calories_kcal.placeholder_text = "kcal"
	sodium_meq = LineEdit.new()
	sodium_meq.connect("text_changed", self, "_bag_changed")
	sodium_meq.placeholder_text = "mEq"
	potassium_meq = LineEdit.new()
	potassium_meq.connect("text_changed", self, "_bag_changed")
	potassium_meq.placeholder_text = "mEq"
	calcium_mg = LineEdit.new()
	calcium_mg.connect("text_changed", self, "_bag_changed")
	calcium_mg.placeholder_text = "mg"
	magnesium_mmol = LineEdit.new()
	magnesium_mmol.connect("text_changed", self, "_bag_changed")
	magnesium_mmol.placeholder_text = "mmol"
	chloride_mmol = LineEdit.new()
	chloride_mmol.connect("text_changed", self, "_bag_changed")
	chloride_mmol.placeholder_text = "mmol"
	phosphorus_mg = LineEdit.new()
	phosphorus_mg.connect("text_changed", self, "_bag_changed")
	phosphorus_mg.placeholder_text = "mg"

func set_expander():
	set_empty()
	title.set_h_size_flags(Control.SIZE_EXPAND | Control.SIZE_FILL)

func set_units():
	set_empty()
	volume_ml.set_text("ml")
	protide_g.set_text("g")
	nitrogen_g.set_text("g")
	carbohydrate_g.set_text("g")
	lipides_g.set_text("g")
	total_calories_kcal.set_text("kcal")
	total_calories_non_prot_kcal.set_text("kcal")
	sodium_meq.set_text("mEq")
	potassium_meq.set_text("mEq")
	calcium_mg.set_text("mg")
	magnesium_mmol.set_text("mmol")
	chloride_mmol.set_text("mmol")
	phosphorus_mg.set_text("mg")

func set_mini():
	set_empty()
	protide_g.set_text("Prot")
	nitrogen_g.set_text("Z")
	carbohydrate_g.set_text("Glu")
	lipides_g.set_text("Lip")
	total_calories_kcal.set_text("Cal")
	total_calories_non_prot_kcal.set_text("Cal N.P.")
	sodium_meq.set_text("Na")
	potassium_meq.set_text("K")
	calcium_mg.set_text("Ca")
	magnesium_mmol.set_text("Mg")
	chloride_mmol.set_text("Cl")
	phosphorus_mg.set_text("P")

func set_titles():
	set_empty()
	brand.set_text("Marques")
	product.set_text("Produits")
	volume_ml.set_text("Volumes")
	protide_g.set_text("Protides")
	nitrogen_g.set_text("Azote")
	carbohydrate_g.set_text("Glucode")
	lipides_g.set_text("Lipides")
	total_calories_kcal.set_text("Calories Totales")
	total_calories_non_prot_kcal.set_text("Cal Totales NP")
	sodium_meq.set_text("Sodium")
	potassium_meq.set_text("Potassium")
	calcium_mg.set_text("Calcium")
	magnesium_mmol.set_text("Magnesium")
	chloride_mmol.set_text("Chlorure")
	phosphorus_mg.set_text("Phosphore")

func _volume_changed(text):
	var v_ml = int(volume_ml.get_text())
	if fluid.volume_ml <= 0:
		return
	for i in all_items:
		if i == "volume_ml" or i == "total_calories_non_prot_kcal":
			continue
		var value = v_ml * fluid.get(i) / fluid.volume_ml
		self.get(i).set_text(Utils.float_print(value))
	
	var cal_np = v_ml * (fluid.carbohydrate_g * 4 + fluid.lipides_g * 9) / fluid.volume_ml
	total_calories_non_prot_kcal.set_text(Utils.float_print(cal_np))
	# Update parent too
	emit_signal("refresh")

func _bag_changed(text):
	var v_ml = int(volume_ml.get_text())
	var cal_np = Utils.floatit(carbohydrate_g.get_text()) * 4 + Utils.floatit(lipides_g.get_text()) * 9
	total_calories_non_prot_kcal.set_text(Utils.float_print(cal_np))
	# Update parent too
	emit_signal("refresh")

func _percentage_changed(text):
	refresh_fortifier()
	emit_signal("refresh")

func refresh_fortifier():
	var text = percentage.get_text()
	var perc = Utils.floatit(text)
	# Search for existing milk
	var milk_item
	for i in all_items_ref:
		if i.fluid.type == "Milk" or i.fluid.type == "MotherMilk":
			milk_item = i
	if typeof(milk_item) == TYPE_NIL:
		return
	var v_ml = int(milk_item.volume_ml.get_text())
	if milk_item.fluid.volume_ml <= 0:
		return

	for i in all_items:
		if i == "volume_ml" or i == "total_calories_non_prot_kcal":
			continue
		var value = v_ml * perc / 100.0 * fluid.get(i) / fluid.weight_g
		self.get(i).set_text(Utils.float_print(value))
	
	var cal_np = v_ml * perc / 100.0 * (fluid.carbohydrate_g * 4 + fluid.lipides_g * 9) / fluid.weight_g
	total_calories_non_prot_kcal.set_text(Utils.float_print(cal_np))

func _close():
	emit_signal("close", self)
