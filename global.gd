# This file is part of OPTIBIB, a software to optimise enteral feeding for newborn babies
# Copyright (C) 2018 Wiem HASSEN JUTTEAU
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node

var _current_scene = null
var _global_patient = null

const VERSION = "18.11"

func _ready():
	goto_patients()

func global_patient():
	return _global_patient

static func user_path():
	return "user://" + VERSION + "/"

static func user_path_real():
	return OS.get_user_data_dir() + "/" + VERSION + "/"

func goto_patients():
	# called later so we do not try to remove the current scene
	# while a function inside it is calling this function (may crash).
	call_deferred("_deferred_goto_patients")

func _deferred_goto_patients():
	if _current_scene != null:
		_current_scene.queue_free()
	var s = ResourceLoader.load("res://Patient/Patients.tscn")
	_current_scene = s.instance()
	get_tree().get_root().add_child(_current_scene)
	get_tree().set_current_scene(_current_scene)

func goto_menu(patient):
	call_deferred("_deferred_goto_menu", patient)

func _deferred_goto_menu(patient):
	_global_patient = patient
	if _current_scene != null:
		_current_scene.queue_free()
	var s = ResourceLoader.load("res://Feed/Menu.tscn")
	_current_scene = s.instance()
	get_tree().get_root().add_child(_current_scene)
	get_tree().set_current_scene(_current_scene)
